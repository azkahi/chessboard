function populateEmpty() {
  let tempRow = [];
  for (let i = 0; i < 8; i++) {
    tempRow.push("-");
  }

  return tempRow;
}

function populatePion(colour) {
  let tempRow = []
  for (let i = 0; i < 8; i++) {
    tempRow.push(`P ${colour}`);
  }

  return tempRow;
}

function populatePieces(colour) {
  return [`B ${colour}`, `K ${colour}`, `M ${colour}`, `RT ${colour}`, `RJ ${colour}`, `M ${colour}`, `K ${colour}`, `B ${colour}`]
}

const makeChessboard = () => {
  let chessboard = []

  // ... write your code here
  for (let i = 0; i < 8; i++) {
    switch(i) {
      case 0:
        // Row 1
        chessboard.push(populatePieces("Black"));
        break;
      case 1:
        // Row 2
        chessboard.push(populatePion("Black"));
        break;
      case 2:
        // Row 3
        chessboard.push(populateEmpty());
        break;
      case 3:
        // Row 4
        chessboard.push(populateEmpty());
        break;
      case 4:
        // Row 5
        chessboard.push(populateEmpty());
        break;
      case 5:
        // Row 6
        chessboard.push(populateEmpty());
        break;
      case 6:
        // Row 7
        chessboard.push(populatePion("White"));
        break;
      case 7:
        // Row 8
        chessboard.push(populatePieces("White"));
        break;
      }
    }

  return chessboard
}

const printBoard = x => {
  // ... write your code here
  for (const rowBoard of x) {
    let strOut = "["
    rowBoard.forEach((piece, index) => {
      if (index != rowBoard.length - 1) {
        strOut += `"${piece}", `
      } else {
        strOut += `"${piece}"]`
      }
    });
    console.log(strOut);
  }
}

const printPos = (x, y, board) => {
  console.log(`"${board[x][y]}"`);
}

printBoard(makeChessboard())

printPos(1, 1, makeChessboard());